![](https://www.ipso.ch/themes/custom/beg/favicons/ibz/apple-touch-icon.png)

# xMas Projektwoche 2021 
###### Sajeela Ahmed
<br/>

## Inhaltsverzeichnis
---
1. [GitHub](#github)
2. [GitLab](#gitlab)
3. [Markdown](#markdown)
4. [DevOps](#devops)
<br/>

### GitHub 
---

##### Was ist GitHub
GitHub ist ein webbasiertes Filehosting-Service für die Softwareentwicklung und das Teilen von Softwarecode. Es wurde vom Unternehmen GitHub erfunden. 

##### Warum GitHub
Bei GitHub kann man einfach Zusammenarbeiten. Sei es in Partnerarbeit, alleine oder in Gruppen. 

##### Welches sind die Vor- und Nachteile
Ein Vorteil von GitHub ist, dass man Partenrarbeiten sehr guet umsetzen kann, auch wenn man nicht gleich nebeneinander sitzt. Dazu kann man auch mit der kostenlosen Version für GitHub sehr gut arbeiten. Man kann sich die Projekte untereinander sehr gut austauschen. Der Nachteil von GitHub ist, dass es Speicherplatzbeschränkungen hat und, dass es nich absolut offen ist. 

##### Wozu braucht man GitHub
Fals man Änderungen an den Projekten im GitHub machen möchte, kann man die neuste Version der Projekt erneut hochladen und die Teammitglieder können sich die neuste Version anschauen und allenfalls Änderungen machen.

##### Wie kann ich davon profitieren
Die Praktikumsstelle, die ich gefunden habe, dort arbeiten sie auch sehr gerne mit GitHub. Ich kenne GitHub schon von anderen Modulen. Ich kann davon sehr gut profitieren, weil ich schon mit GitHub arbeiten kann, somit werde ich auch beim Praktikum keine Probleme haben.

<br/>

### GitLab
---

##### Was ist GitLab
GitLab ist ein weltverbreitetes Versionskontrollsystem. Mit GitLab kann man Softwareprojekte verwalten. 

##### Was bietet GitLab
- Issue-Tracking-System mit Kanban-Borad
- System für Continuous Integration und Continuous Delivery (CI/CD)
- Ein Wiki
- Container-Registry
- Sicherheitsscanner für Container
- Sourcecode
- Multi-Cluster-Verwaltung
- Überwachung

##### Warum GitLab
GitLab ist eine sehr gute Software um Projekte hochzuladen. Speziell ist es, dass man die hochgeladene Versionen anschauen kann.

##### Welches sind die Vor- und Nachteile
Der Vorteil von Gitlab ist, dass es kostenlos ist. Nachteil: Es hat nicht unendlich viel Speicherplatz.

##### Wozu braucht man GitLab
Ähnlich wie bei GitHub kann man hier auch seine Softwareprojekte hochladen und mit anderen teilen. GitLab hat auch ein Wiki, bei dem man vieles nachlesen kann.

##### Wie kann ich davon profitieren
Ähnlich wie bei GitHub kann man hier auch seine Softwareprojekt hochladen und mit anaderen teilen. GitLab hat auch ein Wiki, bei dem man vieles nachlesen kann. 

### Markdown 
---

##### Was ist Markdown
Markdown ist eine Auszeichnungssprache wie HTML. Man kann Texte und Dokumente sehr einfach und formatiert erstellen. Diese Sprache ist von John Gruber und Aaron Swartz entworfen worden. Das Ziel von Markdown ist es, eine leicht lesbare Ausgangsform für Dokumente usw. zu programmieren.

##### Warum Markdown
Wenn man die ganze Zeit nur auf Word seine Dokumentationen macht, wird es manchmal einbischen langweilig. Informatiekr brauchen auch mal einbisschen Abwechslung. In dieser Situation ist Markdown am Besten.

##### Welches sind die Vor- und Nachteile
Es git sehr viele Vorteile von Markdown. Das eine ist. dass es sehr einfach ist zu benutzen. Man braucht nicht mal viel Übung. 5 Minuten und du kannst es! Ein ggrosser Vorteil ist auch, dass die Texte die man schreibt selbst von Markdown formartiert werden. In meiner Sicht, gibt es keine Nachteile von Markdown. Ich mag es sehr...

##### Wozu braucht man Markdown
Man benutzt es um zum Beispiel Dokumente und Webseiten zu formatieren. Man kann sehr lange Texte, Dokumentationen ect. schreiben. Zudem kann man auch schöne Webseiten mit Markdown machen.

##### Wie kann ich davon profitieren
Markdown ist ein sehr gutes Ersatz für Word, wenn man mal genug von Word hat. Zudem ist sehr einfach ein elegantes Dokument zu erstellen. 
<br/>

### DevOps
---

##### Was ist DevOps 
DevOps ist eine Sammlung unterschiedlicher technischer Methoden und eine Kultur zur Zusammenarbeit zwischen Softwareentwicklung und IT-Betrieb. DevOps soll eine effektivere und effiziente Zusammenarbeit der Bereiche Softwareentwicklung, Systemadministratoren aber auch Qualitätssicherung ermöglichen.

##### Warum DevOps 
DevOps ist kostenlos und ermöglicht den Informatikern eine effiziente Zusammenarbeit, egal wie gross die Projekte sind. Speziell wen es sich um Zusammenarbeit zwischen Applikationsentwickler und Systemtechnikern handelt.

##### Welches sind die Vor- und Nachteile
Der Vorteil von DevOps ist, dass man durch DevOps mit Systemtechnikern und Applikationsentwickler schnell und effizient arbeiten kann. Nachteil ist, dass es nicht so einfach ich mit DevOps zu arbeiten. Damit alles mit DevOps gut funktioniert, braucht man gewisse Übung.

##### Wozu braucht man DevOps
Es kann Projekte geben, bei denen Systemtechniker und Applikationsentwickler zusammenarbeiten. Damit es zwischen Applikationsentwickler und Systemtechniker funktioniert, ist DevOps die beste Software. Somit kann man untereinander effizient und schnell Projekte teilen.

##### Wie kann ich davon profitieren
Es kann vorkommen, dass ich in meiner Berufswelt auch mit Systemtechnikern zu tun habe. Es ist dehr gut für mich, dass ich DevOps schon einbischen kenne. Sobald ich Projekte mit Systemtechnikern erarbeiten muss, kann ich nach Gelegenheit, DevOps benutzen.

<br/>